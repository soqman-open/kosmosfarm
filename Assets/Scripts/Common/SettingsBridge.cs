﻿using UnityEngine;

public static class SettingsBridge 
{
    public static Field GetField(string name)
    {
        return Resources.Load<Field>(ConstantValues.FieldSettingsPath + name);
    }
    
    public static ResourceItem GetResourceItem(string name)
    {
        return Resources.Load<ResourceItem>(ConstantValues.ResourceSettingsPath + name);
    }
    
    public static Level GetLevel(string name)
    {
        return Resources.Load<Level>(ConstantValues.LevelSettingsPath + name);
    }
}
