﻿public static class ConstantValues
{
    public const string EmptyFieldName = "Empty";
    public const string VacantFieldName = "Vacant";
    public const string FieldSettingsPath = "Settings/FieldItems/";
    public const string ResourceSettingsPath = "Settings/ResourceItems/";
    public const string LevelSettingsPath = "Settings/Levels/";
    public const string RulesSettingsPath = "Rules/";
    public const string AutoSaveDirectoryPath = "Autosave/";
    public const string BarnSaveFileName = "barn";
    public const string FieldsSaveFileName = "fields";
    public const string LevelSaveFileName = "level";
}
