﻿using System.IO;
using UnityEngine;

public static class SaveLoadUtility 
{
    public static string GetAutoSavePath()
    {
        return Path.Combine(Application.persistentDataPath, ConstantValues.AutoSaveDirectoryPath);
    }

    public static string GetRulesPath()
    {
        return Path.Combine(Application.streamingAssetsPath, ConstantValues.RulesSettingsPath);
    }

    public static void SaveToFile<T>(string fileName, T data)
    {
        Directory.CreateDirectory(GetAutoSavePath());
        File.WriteAllText(Path.Combine(GetAutoSavePath(),fileName),JsonUtility.ToJson(data));
    }

    public static object LoadFromFile<T>(string fileName)
    {
        if (!File.Exists(Path.Combine(GetAutoSavePath(), fileName))) return null;
        return JsonUtility.FromJson<T>(File.ReadAllText(Path.Combine(GetAutoSavePath(),fileName)));
    }
}
