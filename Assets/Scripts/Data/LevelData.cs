﻿using System;

[Serializable]
public class LevelData
{
    public string levelName;

    public LevelData(Level level)
    {
        levelName = level.name;
    }
}