﻿
using System;

[Serializable]
public class MachineData
{
    public Machine.Status status;
    public float progress;
    public MachineData(Machine.Status status, float progress)
    {
        this.status = status;
        this.progress = progress;

    }
}