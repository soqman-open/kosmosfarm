﻿using System;
using System.Collections.Generic;

[Serializable]
public class FieldsData
{
    public List<FieldData> fieldsData;
    public int x;

    public FieldsData(List<FieldHolder> fields, int columns)
    {
        fieldsData=new List<FieldData>();
        foreach (var item in fields)
        {
            fieldsData.Add(item.Save());
        }
        x = columns;
    }
}