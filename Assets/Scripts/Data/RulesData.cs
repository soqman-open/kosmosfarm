﻿using System;

[Serializable]
public class RulesData
{
    public enum ResourceUpdateMode{Add, Replace}
    public ResourceStorageData youNeedIt;
    public ResourceStorageData youNeedItPlanted;
    public string mode;
    public ResourceStorageData startResources;
    public string description;
    
    
}
