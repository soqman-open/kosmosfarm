﻿using System;

[Serializable]
public class BarnData
{
    public ResourceStorageData resourceStorageData;

    public BarnData(ResourceStorageData resourceStorageData)
    {
        this.resourceStorageData = resourceStorageData;
    }
}