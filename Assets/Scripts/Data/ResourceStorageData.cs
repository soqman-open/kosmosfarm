﻿using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class ResourceStorageData
{
    public List<string> keys;
    public List<int> values;

    public ResourceStorageData(Dictionary<string, int> resources)
    {
        keys = resources.Keys.ToList();
        values = resources.Values.ToList();
    }

    public Dictionary<string, int> GetTable()
    {
        var table = new Dictionary<string, int>();
        if (keys==null || keys.Count.Equals(0))
        {
            return table;
        }
        for (var i = 0; i < keys.Count; i++)
        {
            table.Add(keys[i], values[i]);
        }
        return table;
    }

    public string GetFirstKey()
    {
        if(keys==null || keys.Count==0 )return null;
        return keys[0];
    }
    
    public int GetFirstValue()
    {
        if(values==null || values.Count==0 )return 0;
        return values[0];
    }
}