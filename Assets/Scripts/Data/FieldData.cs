﻿using System;
using System.Collections.Generic;

[Serializable]
public class FieldData
{
    public string _fieldItemName;
    public List<MachineData> machineDatas;
    public List<ResourceStorageData> storageDatas;
    public FieldData(Field field)
    {
        _fieldItemName = field.name;
    }

    public void SetMachinesData(List<MachineData> machines)
    {
        machineDatas = machines;
    }
    public void SetStoragesData(List<ResourceStorageData> storages)
    {
        storageDatas = storages;
    }

    public List<MachineData> GetMachinesData()
    {
        return machineDatas;
    }
    
    public List<ResourceStorageData> GetResourceStoragesData()
    {
        return storageDatas;
    }
    public Field GetField()
    {
        return SettingsBridge.GetField(_fieldItemName);
    }
    public string GetFieldName()
    {
        return _fieldItemName;
    }
}