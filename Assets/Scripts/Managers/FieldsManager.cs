﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FieldsManager : Singleton<FieldsManager>
{
    [Header("dependencies")]
    [SerializeField] private FieldHolder fieldPrefab;
    [SerializeField] private FieldHolder emptyPrefab;
    [SerializeField] private FieldHolder vacantPrefab;
    [SerializeField] private GridLayoutGroup grid;
    [Header("status")]
    [SerializeField] private FieldsPack fieldsPack;
    [SerializeField] private List<FieldHolder> currentFields=new List<FieldHolder>();
    private List<OnPlantFieldListener> onPlantFieldListeners=new List<OnPlantFieldListener>();

    public void OnPlantFieldListenerAdd(OnPlantFieldListener listener)
    {
        onPlantFieldListeners.Add(listener);
    }
    
    public void OnPlantFieldListenerRemove(OnPlantFieldListener listener)
    {
        onPlantFieldListeners.Remove(listener);
    }
    public void Init(FieldsPack pack)
    {
        fieldsPack = pack;
        InitLevel();
    }
    public void InitLevel()
    {
        if (fieldsPack == null) return;
        grid.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
        grid.constraintCount = fieldsPack.GetX();
        foreach (var item in fieldsPack.GetFields())
        {
            if (item.name.Equals(ConstantValues.EmptyFieldName))
            {
                currentFields.Add(Instantiate(emptyPrefab,grid.transform));
            }
            /*else if (item.name.Equals(ConstantValues.VacantFieldName))
            {
                currentFields.Add(Instantiate(vacantPrefab,grid.transform));
            }*/
            else
            {
                var f = Instantiate(fieldPrefab, grid.transform);
                f.SetField(item);
                currentFields.Add(f);
            }
        }
    }
    
    public void InitByData(FieldsData data)
    {
        grid.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
        grid.constraintCount = data.x;
        foreach (var item in data.fieldsData)
        {
            if (item.GetFieldName().Equals(ConstantValues.EmptyFieldName))
            {
                currentFields.Add(Instantiate(emptyPrefab,grid.transform));
            }
            /*else if (item.GetFieldName().Equals(ConstantValues.VacantFieldName))
            {
                currentFields.Add(Instantiate(vacantPrefab,grid.transform));
            }*/
            else
            {
                var f = Instantiate(fieldPrefab, grid.transform);
                f.Load(item);
                currentFields.Add(f);
            }
        }
    }
    
    public void Clear()
    {
        foreach (var item in currentFields)
        {
            Destroy(item.gameObject);
        }
        currentFields.Clear();
    }
    
    public FieldsData Save()
    {
        return new FieldsData(currentFields,grid.constraintCount);
    }

    public void Load(FieldsData data)
    {
        Clear();
        InitByData(data);
    }

    public int GetFieldCount(string fieldItemName)
    {
        var count=0;
        foreach (var item in currentFields.Where(x=>x.GetField().name.Equals(fieldItemName)))
        {
            count++;
        }
        return count;
    }

    public void FieldPlanted()
    {
        TriggerOnPlantField();
    }

    public void TriggerOnPlantField()
    {
        foreach (var listener in onPlantFieldListeners)
        {
            listener.OnPlantField();
        }
    }
}
