﻿using System.Collections.Generic;
using UnityEngine;

public class BarnManager : Singleton<BarnManager>
{
    [Header("dependencies")]
    [SerializeField] private ResourceStorage resourceStorage;
    [SerializeField] private Transform resourcesRoot;
    [SerializeField] private ResourceViewUpdater resourceViewPrefab;
    [Header("status")]
    [SerializeField] private ResourcePack currentPack;
    [SerializeField] private List<ResourceViewUpdater> currentResources = new List<ResourceViewUpdater>();
    public void Init(ResourcePack pack)
    {
        currentPack = pack;
        Init();
    }
    public void Init()
    {
        if (currentPack == null) return;
        foreach (var item in currentPack.GetResources())
        {
            var resource = Instantiate(resourceViewPrefab, resourcesRoot);
            resource.SetResourceStorage(resourceStorage);
            resource.UpdateView(item);
            currentResources.Add(resource);
        }
    }

    public List<ResourceItem> GetCurrentResources()
    {
        return currentPack.GetResources();
    }

    public void ClearResources()
    {
        resourceStorage.Reset();
    }

    public void Clear()
    {
        foreach (var item in currentResources)
        {
            Destroy(item.gameObject);
        }
        currentResources.Clear();
    }
    public bool TryToGetResource(ResourceItem resourceItem)
    {
        return resourceStorage.GetResource(resourceItem);
    }
    
    public bool TryToGetResource(ResourceItem resourceItem, int count)
    {
        return resourceStorage.GetResource(resourceItem, count);
    }

    public void Put(ResourceItem resourceItem)
    {
        resourceStorage.PutResource(resourceItem);
    }
    
    public void Put(ResourceItem resourceItem, int count)
    {
        resourceStorage.PutResource(resourceItem, count);
    }
    
    public void Put(string resourceItemName, int count)
    {
        resourceStorage.PutResource(resourceItemName, count);
    }

    public List<Field> GetAvailableFields()
    {
        return resourceStorage.GetFields();
    }

    public BarnData Save()
    {
        return new BarnData(resourceStorage.Save());
    }

    public void Load(BarnData data)
    {
        resourceStorage.Load(data.resourceStorageData);
    }

    public void SetBarnStorage(ResourceViewUpdater viewUpdater)
    {
        if (viewUpdater == null || resourceStorage==null) return;
        viewUpdater.SetResourceStorage(resourceStorage);
    }
}
