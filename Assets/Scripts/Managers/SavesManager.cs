﻿public class SavesManager : Singleton<SavesManager>
{
    public void Save()
    {
        var barnData = BarnManager.Instance.Save();
        SaveLoadUtility.SaveToFile(ConstantValues.BarnSaveFileName,barnData);
        var levelData = LevelManager.Instance.Save();
        SaveLoadUtility.SaveToFile(ConstantValues.LevelSaveFileName,levelData);
        var fieldsData = FieldsManager.Instance.Save();
        SaveLoadUtility.SaveToFile(ConstantValues.FieldsSaveFileName,fieldsData);
    }

    public void Load()
    {
        var levelData = (LevelData) SaveLoadUtility.LoadFromFile<LevelData>(ConstantValues.LevelSaveFileName);
        LevelManager.Instance.Load(levelData);
        var fieldsData = (FieldsData) SaveLoadUtility.LoadFromFile<FieldsData>(ConstantValues.FieldsSaveFileName);
        FieldsManager.Instance.Load(fieldsData);
        var barnData = (BarnData)SaveLoadUtility.LoadFromFile<BarnData>(ConstantValues.BarnSaveFileName);
        BarnManager.Instance.Load(barnData);
    }
    
}
