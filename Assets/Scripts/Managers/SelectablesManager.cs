﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SelectablesManager : Singleton<SelectablesManager>
{
    [SerializeField] private List<SelectableBehaviour> selectables = new List<SelectableBehaviour>();

    public void RegisterSelectable(SelectableBehaviour selectableBehaviour)
    {
        if (!selectables.Contains(selectableBehaviour))
        {
            selectables.Add(selectableBehaviour);
        }
    }
    
    public void UnregisterSelectable(SelectableBehaviour selectableBehaviour)
    {
        selectables.Remove(selectableBehaviour);
    }

    public void OnSelect(SelectableBehaviour selectable)
    {
        foreach (var item in selectables.Where(item => item.GetSelectableGroupTag().Equals(selectable.GetSelectableGroupTag()) && !item.Equals(selectable)) )
        {
            item.Deselect();
        }
    }

    public void DeselectAll()
    {
        foreach (var item in selectables)
        {
            item.Deselect();
        }
    }
    
    public void DeselectAll(string tag)
    {
        foreach (var item in selectables.Where(item => item.GetSelectableGroupTag().Equals(tag)))
        {
            item.Deselect();
        }
    }

    private void Reset()
    {
        selectables.Clear();
    }
}
