﻿using System.Collections.Generic;
using UnityEngine;
public class FieldInspectorManager : Singleton<FieldInspectorManager>
{
    private FieldHolder _currentFieldHolder;

    [SerializeField] private GameObject fieldInspectorRoot;
    [SerializeField] private GameObject fieldPlacerRoot;
    [SerializeField] private GameObject fieldPlacerResourcesRoot;
    [SerializeField] private ResourceViewUpdater fieldToPlacePrefab;
    [SerializeField] private ResourceViewUpdater consumeViewUpdater;
    [SerializeField] private ResourceViewUpdater produceViewUpdater;
    [SerializeField] private ResourceViewUpdater viewUpdaterField;
    [SerializeField] private ProgressViewUpdater consumeProgressViewUpdater;
    [SerializeField] private ProgressViewUpdater produceProgressViewUpdater;
    [SerializeField] private BarnHandler produceBarnHandler;
    [SerializeField] private BarnHandler consumeBarnHandler;
    [SerializeField] private GameObject consumeRoot;
    [SerializeField] private GameObject produceRoot;
    [SerializeField] private ResourceStorage barnStorage;

    private List<ResourceViewUpdater> currentFieldsToPlace=new List<ResourceViewUpdater>();

    public FieldHolder GetCurrentFieldHolder()
    {
        return _currentFieldHolder;
    }
    public void SetFieldHolder(FieldHolder fieldHolder)
    {
        _currentFieldHolder = fieldHolder;
        UpdateView();
    }
    
    public void Deselect()
    {
        _currentFieldHolder = null;
        UpdateView();
    }

    public void UpdateView()
    {
        if (_currentFieldHolder == null || _currentFieldHolder.GetField() == null)
        {
            HideInspector();
            return;
        }
        ShowInspector();
        UpdateInfo();
        UpdateConsume();
        UpdateProduce();
        UpdateBarnHandler();
        UpdatePlacer();
    }

    private void UpdatePlacer()
    {
        if (!_currentFieldHolder.GetField().name.Equals(ConstantValues.VacantFieldName))
        {
            fieldPlacerRoot.SetActive(false);
        }
        else
        {
            fieldPlacerRoot.SetActive(true);
            UpdateAvailableResources();
        }
        
    }
    public void Init(List<Field> fieldsPack)
    {
        foreach (var item in fieldsPack)
        {
            var field = Instantiate(fieldToPlacePrefab, fieldPlacerResourcesRoot.transform);
            field.SetResourceStorage(barnStorage);
            field.UpdateView(item);
            currentFieldsToPlace.Add(field);
        }
    }
    
    public void Clear()
    {
        foreach (var item in currentFieldsToPlace)
        {
            Destroy(item.gameObject);
        }
        currentFieldsToPlace.Clear();
    }
    public void UpdateAvailableResources()
    {
        foreach (var item in currentFieldsToPlace)
        {
            if (barnStorage.GetCount(item.GetResource()) > 0)
            {
                item.gameObject.SetActive(true);
            }
            else
            {
                item.gameObject.SetActive(false);
            }
        }
    }

    private void UpdateInfo()
    {
        viewUpdaterField.UpdateView(_currentFieldHolder.GetField());
    }

    public void HideInspector()
    {
        fieldInspectorRoot.SetActive(false);
    }

    private void ShowInspector()
    {
        fieldInspectorRoot.SetActive(true);
    }

    private void UpdateBarnHandler()
    {
        consumeBarnHandler.SetResourceStorage(_currentFieldHolder.GetConsumeResourceStorage());
        produceBarnHandler.SetResourceStorage(_currentFieldHolder.GetProduceResourceStorage());
    }

    private void UpdateConsume()
    {
        if (_currentFieldHolder.GetField().GetConsumeResource() == null)
        {
            consumeRoot.SetActive(false);
            return;
        }
        consumeRoot.SetActive(true);
        consumeViewUpdater.UpdateView(_currentFieldHolder.GetField().GetConsumeResource());
        consumeViewUpdater.SetResourceStorage(_currentFieldHolder.GetConsumeResourceStorage());
        consumeProgressViewUpdater.SetProcess(_currentFieldHolder.GetConsumeMachine().GetProcess());
    }
    
    private void UpdateProduce()
    {
        if (_currentFieldHolder.GetField().GetProduceResource() == null)
        {
            produceRoot.SetActive(false);
            return;
        }
        produceRoot.SetActive(true);
        produceViewUpdater.UpdateView(_currentFieldHolder.GetField().GetProduceResource());
        produceViewUpdater.SetResourceStorage(_currentFieldHolder.GetProduceResourceStorage());
        produceProgressViewUpdater.SetProcess(_currentFieldHolder.GetProduceMachine().GetProcess());
    }
}
