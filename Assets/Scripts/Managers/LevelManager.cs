﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LevelManager : Singleton<LevelManager>, OnResourceChangedListener, OnPlantFieldListener
{
    [SerializeField] private Level currentLevel;
    [SerializeField] private List<Level> gameLevels;
    [SerializeField] private ResourceStorage barnStorage;
    [SerializeField] private TMP_Text rulesView;
    [SerializeField] private Image background;

    [SerializeField] private UnityEvent OnWinLevel;
    [SerializeField] private UnityEvent OnWinGame;

    private void Awake()
    {
        barnStorage.RegisterOnResourceChangeListener(this);
        FieldsManager.Instance.OnPlantFieldListenerAdd(this);
    }

    private void OnDestroy()
    {
        if(barnStorage!=null)barnStorage.UnregisterOnResourceChangedListener(this);
        if(FieldsManager.Instance!=null) FieldsManager.Instance.OnPlantFieldListenerRemove(this);
    }

    private void Start()
    {
        currentLevel = gameLevels[0];
        Init();
    }
    public void Init()
    {
        if (currentLevel == null) return;
        FieldsManager.Instance.Init(currentLevel.GetFieldsPack());
        FieldInspectorManager.Instance.Init(currentLevel.GetResourcesPack().GetResources().OfType<Field>().ToList());
        StoreManager.Instance.Init(currentLevel.GetProductsPack());
        BarnManager.Instance.Init(currentLevel.GetResourcesPack());
        UpdateRulesView();
        background.color = currentLevel.GetColor();
        AddResources();
    }
    
    public void InitByData(Level level)
    {
        currentLevel = level;
        FieldInspectorManager.Instance.Init(currentLevel.GetResourcesPack().GetResources().OfType<Field>().ToList());
        StoreManager.Instance.Init(currentLevel.GetProductsPack());
        BarnManager.Instance.Init(currentLevel.GetResourcesPack());
        UpdateRulesView();
        background.color = currentLevel.GetColor();
    }

    private void AddResources()
    {
        if (!String.IsNullOrEmpty(currentLevel.GetRules().mode) && currentLevel.GetRules().mode.Equals(RulesData.ResourceUpdateMode.Replace.ToString()))
        {
            BarnManager.Instance.ClearResources();
        }
        foreach (var item in currentLevel.GetRules().startResources.GetTable())
        {
            BarnManager.Instance.Put(item.Key,item.Value);
        }
    }
    
    public void Clear(bool clearResource)
    {
        FieldsManager.Instance.Clear();
        StoreManager.Instance.Clear();
        BarnManager.Instance.Clear();
        FieldInspectorManager.Instance.Clear();
        if(clearResource)BarnManager.Instance.ClearResources();
        rulesView.text = "";
    }
    public LevelData Save()
    {
        return new LevelData(currentLevel);
    }

    public void Load(LevelData data)
    {
        Clear(true);
        InitByData(SettingsBridge.GetLevel(data.levelName));
    }

    public void NextLevel()
    {
        Clear(false);
        currentLevel = gameLevels[GetNextLevelIndex()];
        Init();
    }

    private int GetNextLevelIndex()
    {
        var index = gameLevels.FindIndex(x=>x.name==currentLevel.name);
        if (index < 0) index = 0;
        if (index >= gameLevels.Count - 1) index = 0;
        return index;
    }

    private void WinGame()
    {
        OnWinGame.Invoke();
    }
    
    private void WinLevel()
    {
        if (GetNextLevelIndex()==0)
        {
            WinGame();
            return;
        }
        if(FieldInspectorManager.Instance!=null)FieldInspectorManager.Instance.HideInspector();
        if (StoreManager.Instance != null)StoreManager.Instance.Hide();
        OnWinLevel.Invoke();
    }

    public void Reset()
    {
        Clear(true);
        currentLevel = gameLevels[0];
        Init();
    }

    public void OnResourceChanged()
    {
        CheckRules();
    }

    private void CheckRules()
    {
        if (currentLevel.GetRules() == null) return;
        var youNeedIt = currentLevel.GetRules().youNeedIt.GetTable();
        if (youNeedIt != null)
        {
            foreach (var item in youNeedIt)
            {
                if (barnStorage.GetCount(item.Key) < item.Value) return;
            }
        }
        var youNeedItPlanted = currentLevel.GetRules().youNeedItPlanted.GetTable();
        if (youNeedItPlanted != null)
        {
            foreach (var item in youNeedItPlanted)
            {
                if (FieldsManager.Instance.GetFieldCount(item.Key) < item.Value) return;
            }
        }
        WinLevel();
    }

    private void UpdateRulesView()
    {
        rulesView.text = currentLevel.GetRules()?.description;
    }

    public void OnPlantField()
    {
        CheckRules();
    }
}
