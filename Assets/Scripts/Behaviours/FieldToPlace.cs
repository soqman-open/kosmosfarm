﻿using UnityEngine;
public class FieldToPlace : MonoBehaviour
{
    [SerializeField] private ResourceViewUpdater resourceViewUpdater;
    
    public void Place()
    {
        if (FieldInspectorManager.Instance == null || BarnManager.Instance==null) return;
        var fieldHolder = FieldInspectorManager.Instance.GetCurrentFieldHolder();
        var resource = resourceViewUpdater.GetResource();
        if (resource==null || (!resource.GetType().IsSubclassOf(typeof(Field))&& resource.GetType()!=typeof(Field))) return;
        var field = (Field)resource;
        if (fieldHolder == null || fieldHolder.GetField() == null) return;
        if (BarnManager.Instance.TryToGetResource(field))
        {
            field = TryPickVariant(field);
            fieldHolder.SetField(field);
            FieldInspectorManager.Instance.UpdateView();
        }
    }

    private Field TryPickVariant(Field field)
    {
        return field.GetType() == typeof(FieldVariantsWrapper) ? ((FieldVariantsWrapper) field).GetRandomVariant() : field;
    }
}
