﻿using UnityEngine;

public class Timer : Process
{
    private float time;
    private float leftTime;
    public override void Seek(float value)
    {
        base.Seek(value);
        leftTime = Mathf.Lerp(time, 0, Progress);
    }
    public void SetTime(float sec)
    {
        time = sec;
    }

    private void Update()
    {
        if (!IsActive()) return;
        if (leftTime>0)
        {
            leftTime -= Time.deltaTime;
            Progress = Mathf.InverseLerp(time, 0, leftTime);
        }
        else
        {
            leftTime = 0;
            Progress = 1;
            End();
        }
    }
}