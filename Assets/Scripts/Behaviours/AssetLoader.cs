﻿using System.IO;
using System.Threading.Tasks;
using UnityEngine;

public class AssetLoader : MonoBehaviour
{
    [SerializeField] private string gameAssetFileName;
    [SerializeField] private string gameAssetName;
    
    void Start()
    {
        LoadAsset();   
    }

    private async void LoadAsset()
    {
        var asset = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, gameAssetFileName));
        if (asset == null) return;
        var request = asset.LoadAssetAsync<GameObject>(gameAssetName);
        while (request.isDone)
        {
            await Task.Yield();
        }
        Instantiate(request.asset);
        asset.Unload(false);
        Destroy(gameObject);
    }
}