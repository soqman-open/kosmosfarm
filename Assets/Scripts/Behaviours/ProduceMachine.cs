﻿using UnityEngine;
public class ProduceMachine : Machine, OnResourceChangedListener
{
    [Header("dependencies")]
    [SerializeField] private ResourceStorage resourceStorage;

    [Header("settings")]
    [SerializeField] private ResourceItem produceResource;
    [SerializeField] private Field field;
    private void Awake()
    {
        if(resourceStorage!=null) resourceStorage.RegisterOnResourceChangeListener(this);
    }

    private void OnDestroy()
    {
        if(resourceStorage!=null) resourceStorage.UnregisterOnResourceChangedListener(this);
    }
    protected override bool CheckLinkedMachine()
    {
        return linkedMachine == null || linkedMachine.GetStatus() == Status.On;
    }

    protected override bool CheckConditions()
    {
        return resourceStorage != null && produceResource != null;
    }

    protected override bool PreWork()
    {
        return resourceStorage.GetCapacity()>resourceStorage.GetCount(produceResource);
    }

    protected override void SetProcessParams()
    {
        if (process is Timer && produceResource!=null)
        { 
            ((Timer)process).SetTime(produceResource.GetBaseProducingTime()*field.GetProductivityCoefficient());
        }
    }
    protected override bool PostWork()
    {  
        return resourceStorage.PutResource(produceResource);
    }
    public void Init(Field field)
    {
        this.field = field;
        produceResource = field.GetProduceResource();
        resourceStorage.SetCapacity(field.GetProductCapacity());
        Kick();
    }

    public void OnResourceChanged()
    {
        if (GetStatus() == Status.Undefined) return;
        Kick();
    }
}
