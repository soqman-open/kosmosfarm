﻿using System.Collections.Generic;
using UnityEngine;

public class MultipleResourceStorage : ResourceStorage
{
    private Dictionary<string, int> table = new Dictionary<string, int>();
    public override bool PutResource(ResourceItem resource)
    {
        return PutResource(resource.name);
    }
    public override bool PutResource(string resourceItemName)
    {
        if (capacity <= GetCount(resourceItemName)) return false;
        if (table.ContainsKey(resourceItemName))
        {
            table[resourceItemName]++;
        }
        else
        {
            table.Add(resourceItemName,1);
        }
        TriggerChangeResource();
        return true;
    }

    public override bool PutResource(ResourceItem resource, int count)
    {
        return PutResource(resource.name,count);
    }
    public override bool PutResource(string resourceItemName, int count)
    {
        if (capacity < GetCount(resourceItemName)+count) return false;
        if (table.ContainsKey(resourceItemName))
        {
            table[resourceItemName]+=count;
        }
        else
        {
            table.Add(resourceItemName,count);
        }
        TriggerChangeResource();
        return true;
    }

    public override bool GetResource(string resourceItemName)
    {
        if (table.ContainsKey(resourceItemName) && table[resourceItemName]>0)
        {
            table[resourceItemName]--;
            if (table[resourceItemName].Equals(0)) table.Remove(resourceItemName);
            TriggerChangeResource();
            return true;
        }
        return false;
    }

    public override bool GetResource(ResourceItem resource)
    {
        return GetResource(resource.name);
    }

    public override bool GetResource(string resourceItemName, int count)
    {
        if (table.ContainsKey(resourceItemName) && table[resourceItemName]>=count)
        {
            table[resourceItemName]-=count;
            if (table[resourceItemName].Equals(0)) table.Remove(resourceItemName);
            TriggerChangeResource();
            return true;
        }
        return false;
    }

    public override bool GetResource(ResourceItem resource, int count)
    {
        return GetResource(resource.name, count);
    }

    public override int GetAll(string resourceItemName)
    {
        if (table.ContainsKey(resourceItemName) && table[resourceItemName] > 0)
        {
            var count = table[resourceItemName];
            table.Remove(resourceItemName);
            TriggerChangeResource();
            return count;
        }
        return 0;
    }

    public override int GetAll(ResourceItem resource)
    {
        return GetAll(resource.name);
    }

    public override ResourceStorageData Save()
    {
        return table.Count > 0 ? new ResourceStorageData(table) : null;
    }

    public override void Load(ResourceStorageData data)
    {
        if (data.GetTable() == null || data.GetTable().Count == 0) return;
        table = new Dictionary<string, int>(data.GetTable());
        TriggerChangeResource();
    }

    public override int GetCount(ResourceItem resource)
    {
        if (table.ContainsKey(resource.name) && table[resource.name] > 0)
        {
            return table[resource.name];
        }
        return 0;
    }

    public override int GetCount(string resourceName)
    {
        if (table.ContainsKey(resourceName) && table[resourceName] > 0)
        {
            return table[resourceName];
        }
        return 0;
    }

    public override void Reset()
    {
        table.Clear();
        TriggerChangeResource();
    }

    public override List<Field> GetFields()
    {
        var fields = new List<Field>();
        foreach (var item in table)
        {
            var field = SettingsBridge.GetField(item.Key);
            if(field!=null && item.Value>0)fields.Add(field);
        }
        return fields;
    }
}
