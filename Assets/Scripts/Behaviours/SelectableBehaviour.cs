﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class SelectableBehaviour : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] private UnityEvent OnSelect;
    [SerializeField] private UnityEvent OnDeselect;
    [SerializeField] private string selectableGroupTag;
    [SerializeField] private bool selectOnStart;

    private bool isSelected;

    private void Start()
    {
        if (selectOnStart) Select();
    }

    private void Awake()
    {
        SelectablesManager.Instance.RegisterSelectable(this);
    }

    private void OnDestroy()
    {
        if (SelectablesManager.Instance == null) return;
        SelectablesManager.Instance.UnregisterSelectable(this);
    }

    public string GetSelectableGroupTag()
    {
        return selectableGroupTag;
    }

    public bool IsSelected()
    {
        return isSelected;
    }

    public void Select()
    {
        if (isSelected) return;
        isSelected = true;
        SelectablesManager.Instance.OnSelect(this);
        OnSelect.Invoke();
    }

    public void Deselect()
    {
        if (!isSelected) return;
        isSelected = false;
        OnDeselect.Invoke();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Select();
    }
}
