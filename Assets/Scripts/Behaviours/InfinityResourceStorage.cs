﻿using System.Collections.Generic;
using System.Linq;

public class InfinityResourceStorage : ResourceStorage
{
    public override bool PutResource(ResourceItem resource)
    {
        return true;
    }

    public override bool PutResource(string resourceItemName)
    {
        return true;
    }

    public override bool PutResource(ResourceItem resource, int count)
    {
        return true;
    }

    public override bool PutResource(string resourceItemName, int count)
    {
        return true;
    }

    public override bool GetResource(string resourceItemName)
    {
        return true;
    }

    public override bool GetResource(ResourceItem resource)
    {
        return true;
    }

    public override bool GetResource(string resourceItemName, int count)
    {
        return true;
    }

    public override bool GetResource(ResourceItem resource, int count)
    {
        return true;
    }

    public override int GetAll(string resourceItemName)
    {
        return 99999;
    }

    public override int GetAll(ResourceItem resource)
    {
        return 99999;
    }

    public override ResourceStorageData Save()
    {
        return null;
    }

    public override void Load(ResourceStorageData data)
    {
        return;
    }

    public override int GetCount(ResourceItem resource)
    {
        return 99999;
    }

    public override int GetCount(string resourceName)
    {
        return 99999;
    }

    public override void Reset()
    {
        
    }

    public override List<Field> GetFields()
    {
        if (BarnManager.Instance == null) return null;
        var currentResources = BarnManager.Instance.GetCurrentResources();
        return currentResources.OfType<Field>().ToList();
    }
}
