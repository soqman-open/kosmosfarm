﻿using System;
using UnityEngine;
public abstract class Process : MonoBehaviour
{
    protected float Progress;
    private Action _currentEndAction;
    private bool _isActive;
    public float GetProgress()
    {
        return Progress;
    }
    public bool IsActive()
    {
        return _isActive;
    }
    public virtual void Seek(float value)
    {
        if (value < 0) value = 0f;
        if (value > 1) value = 1f;
        Progress = value;
    }
    protected void End()
    {
        _isActive = false;
        _currentEndAction.Invoke();
    }

    public void SetAction(Action onEndProcess)
    {
        _currentEndAction = onEndProcess;
    }

    public void Activate()
    {
        _isActive = true;
    }

    public void Deactivate()
    {
        _isActive = false;
    }
}
