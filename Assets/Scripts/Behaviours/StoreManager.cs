﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreManager : Singleton<StoreManager>, OnResourceChangedListener
{
    [Header("dependencies")]
    [SerializeField] private ResourceViewUpdater productResourceViewUpdater;
    [SerializeField] private Transform productsRoot;
    [SerializeField] private ResourceViewUpdater productPrefab;
    [SerializeField] private CanvasGroup root;
    [SerializeField] private ResourceStorage barnStorage;
    [SerializeField] private ResourceItem exchangeResource;
    [SerializeField] private Button buy;
    [SerializeField] private Button sold;

    [Header("status")]
    [SerializeField] private ResourcePack resourcePack;
    [SerializeField] private ResourceItem currentResourceItem;
    [SerializeField] private List<ResourceViewUpdater> currentProducts=new List<ResourceViewUpdater>();

    private void Awake()
    {
        barnStorage.RegisterOnResourceChangeListener(this);
    }

    private void OnDestroy()
    {
        if(barnStorage!=null)
        barnStorage.UnregisterOnResourceChangedListener(this);
    }

    public void Init(ResourcePack pack)
    {
        resourcePack = pack;
        Init();
    }

    public void Init()
    {
        if (resourcePack == null) return;
        foreach (var item in resourcePack.GetResources())
        {
            var product = Instantiate(productPrefab, productsRoot);
            product.UpdateView(item);
            currentProducts.Add(product);
        }
    }

    public void Clear()
    {
        foreach (var item in currentProducts)
        {
            Destroy(item.gameObject);
        }
        currentProducts.Clear();
    }

    public ResourceItem GetCurrentItem()
    {
        return currentResourceItem;
    }
    public void SetResourceItem(ResourceItem resourceItem)
    {
        currentResourceItem = resourceItem;
        UpdateView();
    }

    private void UpdateView()
    {
        if (productResourceViewUpdater==null || currentProducts!=null && currentProducts.Count==0) return;
        if (currentResourceItem == null)
        {
            currentProducts[0].GetComponent<SelectableBehaviour>().Select();
        }
        productResourceViewUpdater.UpdateView(currentResourceItem);
        if (barnStorage.GetCount(exchangeResource) < currentResourceItem.GetCost()) buy.interactable = false;
        else buy.interactable = true;
        if (barnStorage.GetCount(currentResourceItem) == 0) sold.interactable = false;
        else sold.interactable = true;
    }

    public void Hide()
    {
        root.alpha = 0;
        root.blocksRaycasts = false;
    }

    public void Show()
    {
        root.alpha = 1;
        root.blocksRaycasts = true;
        UpdateView();
    }

    public ResourceItem GetExchangeResource()
    {
        return exchangeResource;
    }

    public void OnResourceChanged()
    {
        UpdateView();
    }
}
