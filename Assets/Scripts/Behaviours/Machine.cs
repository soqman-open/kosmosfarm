﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Machine : MonoBehaviour, OnMachineStatusChangedListener
{ 
    public enum Status {Off, Pause, On, Undefined}
    private Status _currentStatus;
    private readonly List<OnMachineStatusChangedListener> _listeners = new List<OnMachineStatusChangedListener>();
    [SerializeField] protected Process process;
    [SerializeField] private bool loop;
    [SerializeField] private bool autostart;
    [SerializeField]
    protected Machine linkedMachine;

    private void Start()
    {
        if (autostart) Kick();
    }

    public void LinkMachine(Machine machine)
    {
        if (linkedMachine != null)
        {
            linkedMachine.UnregisterOnMachineStatusChangedListener(this);
        }
        linkedMachine = machine;
        if (linkedMachine != null)
        {
            linkedMachine.RegisterOnMachineStatusChangedListener(this);
        }
    }
    public Status GetStatus()
    {
        return _currentStatus;
    }

    public Process GetProcess()
    {
        return process;
    }
    public void Kick()
    {
        switch (_currentStatus)
        {
            case Status.Off:
                _currentStatus = Status.Undefined;
                SetProcessParams();
                process.Seek(0);
                process.SetAction(OnEndProcess);
                if (CheckConditions() && CheckLinkedMachine() && PreWork())
                {
                    process.Activate();
                    _currentStatus = Status.On;
                }
                else
                {
                    _currentStatus = Status.Off;
                }
                break;
            case Status.Pause:
                _currentStatus = Status.Undefined;
                if (CheckConditions() && CheckLinkedMachine())
                {
                    process.Activate();
                    _currentStatus = Status.On;
                }
                else
                {
                    process.Deactivate();
                    _currentStatus = Status.Pause;
                }
                break;
            case Status.On:
                _currentStatus = Status.Undefined;
                if (CheckConditions() && CheckLinkedMachine())
                {
                    process.Activate();
                    _currentStatus = Status.On;
                }
                else
                {
                    process.Deactivate();
                    _currentStatus = Status.Pause;
                }
                break;
        }
        TriggerMachineStatusChanged();
    }
    protected abstract bool CheckLinkedMachine();
    protected abstract bool CheckConditions();
    protected abstract bool PreWork();
    protected abstract void SetProcessParams();
    private void OnEndProcess()
    {
        _currentStatus = Status.Undefined;
        if (PostWork())
        {
            _currentStatus = Status.Off;
            if(loop)Kick();
        }
        else
        {
            _currentStatus = Status.Pause;
        }
        TriggerMachineStatusChanged();
    }

    protected abstract bool PostWork();
    
    public void RegisterOnMachineStatusChangedListener(OnMachineStatusChangedListener listener)
    {
        _listeners.Add(listener);
    }
    public void UnregisterOnMachineStatusChangedListener(OnMachineStatusChangedListener listener)
    {
        _listeners.Remove(listener);
    }

    private void TriggerMachineStatusChanged()
    {
        StartCoroutine(LateTrigger());
    }

    private IEnumerator LateTrigger()
    {
        yield return new WaitForEndOfFrame();
        foreach (var item in _listeners)
        {
            item.OnMachineStatusChanged();
        }
    }
    public MachineData Save()
    {
        return new MachineData(_currentStatus, process.GetProgress());
    }

    public void Load(MachineData data)
    {
        if (data == null) return;
        _currentStatus = data.status;
        process.SetAction(OnEndProcess);
        SetProcessParams();
        process.Seek(data.progress);
        switch (_currentStatus)
        {
            case Status.On:
                process.Activate();
                break;
            case Status.Pause:
            case Status.Off:
                process.Deactivate();
                break;
        }
    }
    public void OnMachineStatusChanged()
    {
        StartCoroutine(KickWithLatency());
    }

    private IEnumerator KickWithLatency()
    {
        yield return new WaitForSeconds(1);
        Kick();
    }
}
