﻿using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ResourceViewUpdater : MonoBehaviour, OnResourceChangedListener
{
    public enum EmptyMode{ Always, Deactivate, Hide, Shade}
    [Header("dependencies")] 
    [SerializeField] private ResourceStorage resourceStorage;
    [SerializeField] private ResourceItem currentResourceItem;
    [SerializeField] private CanvasGroup canvasGroup;
    
    [Header("settings")] 
    [SerializeField] private EmptyMode mode;

    [Header("optional dependencies")]
    [SerializeField] private TMPro.TMP_Text label;
    [SerializeField] private TMPro.TMP_Text count;
    [SerializeField] private TMPro.TMP_Text countMax;
    [SerializeField] private Image picS;
    [SerializeField] private Image picM;
    [SerializeField] private Image picL;
    [SerializeField] private TMPro.TMP_Text cost;
    [SerializeField] private TMPro.TMP_Text costForSell;

    [Header("events")] [SerializeField] private UnityEvent OnCountUpdate;
    
    private int currentCount;

    private void Awake()
    {
        AddListeners();
    }

    private void AddListeners()
    {
        if (resourceStorage != null)
        {
            resourceStorage.RegisterOnResourceChangeListener(this);
        }
    }

    private void RemoveListeners()
    {
        if (resourceStorage != null)
        {
            resourceStorage.UnregisterOnResourceChangedListener(this);
        }
    }

    private void Start()
    {
        if (currentResourceItem != null)
        {
            UpdateView(true);
        }
    }
    public void UpdateView(bool callEvent)
    {
        if (currentResourceItem == null) return;
        UpdatePicL();
        UpdatePicM();
        UpdatePicS();
        UpdateCost();
        UpdateCostForSell();
        UpdateCount(callEvent);
        UpdateLabel();
    }

    private void CheckEmpty()
    {
        if (resourceStorage == null) return;
        if (resourceStorage.GetCount(currentResourceItem) > 0)
        {
            gameObject.SetActive(true);
            canvasGroup.alpha = 1;
        }
        else
        {
            switch (mode)
            {
                case EmptyMode.Always:
                    gameObject.SetActive(true);
                    canvasGroup.alpha = 1;
                    break;
                case EmptyMode.Deactivate:
                    gameObject.SetActive(false);
                    canvasGroup.alpha = 1;
                    break;
                case EmptyMode.Hide:
                    gameObject.SetActive(true);
                    canvasGroup.alpha = 0;
                    break;
                case EmptyMode.Shade:
                    gameObject.SetActive(true);
                    canvasGroup.alpha = 0.5f;
                    break;
            }
        }
    }

    private void UpdateCapacity()
    {
        if (countMax != null) countMax.text = "/" + resourceStorage.GetCapacity();
    }

    public void SetResourceStorage(ResourceStorage resourceStorage)
    {
        RemoveListeners();
        this.resourceStorage = resourceStorage;
        AddListeners();
        UpdateCount(false);
    }
    public void UpdateView(ResourceItem resourceItem)
    {
        currentResourceItem = resourceItem;
        UpdateCapacity();
        UpdateView(false);
    }

    private void UpdateLabel()
    {
        if (label != null) label.text = currentResourceItem.name;
    }

    private void UpdateCount(bool callEvent)
    {
        if (count != null && resourceStorage != null && currentResourceItem!=null)
        {
            var newCount = resourceStorage.GetCount(currentResourceItem);
            if (!newCount.Equals(currentCount))
            {
                currentCount = newCount;
                count.text = currentCount.ToString();
                if(callEvent && isActiveAndEnabled)OnCountUpdate.Invoke();
            }
            CheckEmpty();
        }
    }
    
    private void UpdateCost()
    {
        if (cost != null) cost.text = currentResourceItem.GetCost().ToString(CultureInfo.InvariantCulture);
    }
    
    private void UpdateCostForSell()
    {
        if (costForSell != null) costForSell.text = currentResourceItem.GetCostForSale().ToString(CultureInfo.InvariantCulture);
    }

    private void UpdatePicS()
    {
        if (picS != null) picS.sprite=currentResourceItem.GetPicS();
    }
    
    private void UpdatePicM()
    {
        if (picM != null) picM.sprite=currentResourceItem.GetPicM();
    }
    
    private void UpdatePicL()
    {
        if (picL != null) picL.sprite=currentResourceItem.GetPicL();
    }

    public void OnResourceChanged()
    {
        UpdateCount(true);
    }

    public ResourceItem GetResource()
    {
        return currentResourceItem;
    }
}
