﻿using System.Collections.Generic;
using UnityEngine;

public abstract class ResourceStorage : MonoBehaviour
{
    private List<OnResourceChangedListener> onResourceChangedListeners=new List<OnResourceChangedListener>();
    
    [SerializeField] protected int capacity;
    public abstract bool PutResource(ResourceItem resource);
    public abstract bool PutResource(string resourceItemName);
    public abstract bool PutResource(ResourceItem resource, int count);
    public abstract bool PutResource(string resourceItemName, int count);
    public abstract bool GetResource(string resourceItemName);
    public abstract bool GetResource(string resourceItemName, int count);
    public abstract bool GetResource(ResourceItem resource);
    public abstract bool GetResource(ResourceItem resource, int count);
    public abstract int GetAll(string resourceItemName);
    public abstract int GetAll(ResourceItem resource);
    public abstract ResourceStorageData Save();
    public abstract void Load(ResourceStorageData data);
    public abstract int GetCount(ResourceItem resource);
    public abstract int GetCount(string resourceName);
    public abstract void Reset();
    public int GetCapacity()
    {
        return capacity;
    }
    public abstract List<Field> GetFields();
    public void SetCapacity(int value)
    {
        capacity = value;
    }
    public void RegisterOnResourceChangeListener(OnResourceChangedListener listener)
    {
        onResourceChangedListeners.Add(listener);
    }

    public void UnregisterOnResourceChangedListener(OnResourceChangedListener listener)
    {
        onResourceChangedListeners.Remove(listener);
    }
    public void TriggerChangeResource()
    {
        foreach (var item in onResourceChangedListeners)
        {
            item.OnResourceChanged();
        }
    }
}
