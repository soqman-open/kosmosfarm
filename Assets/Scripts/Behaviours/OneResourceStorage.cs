﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class OneResourceStorage : ResourceStorage
{
    [SerializeField] private int currentCount;
    [SerializeField] private string currentResource;
    public override bool PutResource(ResourceItem resource)
    {
        return PutResource(resource.name);
    }

    public override bool PutResource(string resourceItemName)
    {
        if (capacity <= GetCount(resourceItemName)) return false;
        if (string.IsNullOrEmpty(currentResource))
        {
            currentResource = resourceItemName;
            currentCount=1;
        }else if (resourceItemName.Equals(currentResource))
        {
            currentCount++;
        }
        TriggerChangeResource();
        return true;
    }

    public override bool PutResource(ResourceItem resource, int count)
    {
        return PutResource(resource.name,count);
    }

    public override bool PutResource(string resourceItemName, int count)
    {
        if (capacity < GetCount(resourceItemName)+count) return false;
        if (string.IsNullOrEmpty(currentResource))
        {
            currentResource =resourceItemName;
            currentCount=count;
        }else if (resourceItemName.Equals(currentResource))
        {
            currentCount+=count;
        }
        TriggerChangeResource();
        return true;
    }

    public override bool GetResource(string resourceItemName)
    {
        if (string.IsNullOrEmpty(currentResource) || !resourceItemName.Equals(currentResource) || currentCount<=0)
        {
            return false;
        }
        currentCount--;
        TriggerChangeResource();
        if (currentCount == 0) currentResource = null;
        return true;
    }

    public override bool GetResource(ResourceItem resource)
    {
        return GetResource(resource.name);
    }

    public override bool GetResource(string resourceItemName, int count)
    {
        if (string.IsNullOrEmpty(currentResource) || !resourceItemName.Equals(currentResource) || currentCount<=0)
        {
            return false;
        }
        currentCount--;
        TriggerChangeResource();
        if (currentCount == 0) currentResource = null;
        return true;
    }

    public override bool GetResource(ResourceItem resource, int count)
    {
        return GetResource(resource.name, count);
    }

    public override int GetAll(string resourceItemName)
    {
        if (string.IsNullOrEmpty(currentResource) || !resourceItemName.Equals(currentResource) || currentCount<=0)
        {
            return 0;
        }
        var count = currentCount;
        currentCount = 0;
        TriggerChangeResource();
        return count;
    }

    public override int GetAll(ResourceItem resource)
    {
        return GetAll(resource.name);
    }

    public override ResourceStorageData Save()
    {
        if (string.IsNullOrEmpty(currentResource) || currentCount == 0)
        {
            return null;
        } 
        return new ResourceStorageData(new Dictionary<string, int>()
        {
            {
                currentResource, currentCount
            }
        });
    }

    public override void Load(ResourceStorageData data)
    {
        currentResource = data.GetFirstKey();
        currentCount = data.GetFirstValue();
        TriggerChangeResource();
    }

    public override int GetCount(ResourceItem resource)
    {
        if (string.IsNullOrEmpty(currentResource) || resource==null || !resource.name.Equals(currentResource) || currentCount<=0)
        {
            return 0;
        }
        return currentCount;
    }

    public override int GetCount(string resourceName)
    {
        if (string.IsNullOrEmpty(currentResource) || string.IsNullOrEmpty(resourceName) || !resourceName.Equals(currentResource) || currentCount<=0)
        {
            return 0;
        }
        return currentCount;
    }

    public override void Reset()
    {
        currentCount = 0;
        currentResource = null;
        TriggerChangeResource();
    }

    public override List<Field> GetFields()
    {
        if (currentResource.GetType() != typeof(Field)) return null;
        var field = SettingsBridge.GetField(currentResource);
        if (field == null) return null;
        return new List<Field>(){field};
    }
}
