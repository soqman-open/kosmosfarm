﻿using UnityEngine;

public class FieldInspectorHandler : MonoBehaviour
{
    [SerializeField] private FieldHolder fieldHolder;

    public void OnSelect()
    {
        FieldInspectorManager.Instance.SetFieldHolder(fieldHolder);
    }

    public void OnDeselect()
    {
        
    }

    public void Deselect()
    {
        
    }
}
