﻿using System.Text;
using UnityEngine;

public class CountViewUpdater : MonoBehaviour
{
    [SerializeField] private TMPro.TMP_Text text;
    [SerializeField] private string prefix;
    [SerializeField] private string postfix;
    
    public void UpdateView(int count)
    {
        var result = new StringBuilder(postfix);
        result.Append(count);
        result.Append(postfix);
        text.text = result.ToString();
    }
}
