﻿using UnityEngine;
public class ProductHandler : MonoBehaviour
{
    [SerializeField] private ResourceViewUpdater resourceItem;

    public void SetProduct()
    {
        if(StoreManager.Instance!=null)
        StoreManager.Instance.SetResourceItem(resourceItem.GetResource());
    }
}
