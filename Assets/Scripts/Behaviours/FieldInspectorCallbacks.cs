﻿using UnityEngine;

public class FieldInspectorCallbacks : MonoBehaviour
{
    [SerializeField] private BarnHandler consumeBarnHandler;
    [SerializeField] private BarnHandler produceBarnHandler;
    
    public void Add()
    {
        if (FieldInspectorManager.Instance == null) return;
        var fieldHolder = FieldInspectorManager.Instance.GetCurrentFieldHolder();
        if (fieldHolder == null || fieldHolder.GetField() == null) return;
        consumeBarnHandler.GetFromBarn(fieldHolder.GetField().GetConsumeResource());
    }

    public void Add5()
    {
        if (FieldInspectorManager.Instance == null) return;
        var fieldHolder = FieldInspectorManager.Instance.GetCurrentFieldHolder();
        if (fieldHolder == null || fieldHolder.GetField() == null) return;
        consumeBarnHandler.GetFromBarn(fieldHolder.GetField().GetConsumeResource(),5);
    }

    public void Get()
    {
        if (FieldInspectorManager.Instance == null) return;
        var fieldHolder = FieldInspectorManager.Instance.GetCurrentFieldHolder();
        if (fieldHolder == null || fieldHolder.GetField() == null) return;
        produceBarnHandler.PutToBarn(fieldHolder.GetField().GetProduceResource());
    }

    public void GetAll()
    {
        if (FieldInspectorManager.Instance == null) return;
        var fieldHolder = FieldInspectorManager.Instance.GetCurrentFieldHolder();
        if (fieldHolder == null || fieldHolder.GetField() == null) return;
        produceBarnHandler.PutToBarnAll(fieldHolder.GetField().GetProduceResource());
    }
}
