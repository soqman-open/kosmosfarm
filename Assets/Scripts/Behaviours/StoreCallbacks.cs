﻿using UnityEngine;

public class StoreCallbacks : MonoBehaviour
{
    public void Buy()
    {
        if(StoreManager.Instance==null || BarnManager.Instance==null) return;
        var item = StoreManager.Instance.GetCurrentItem();
        if (item == null) return;
        if (BarnManager.Instance.TryToGetResource(StoreManager.Instance.GetExchangeResource(), item.GetCost()))
        {
            BarnManager.Instance.Put(item);
            if(FieldInspectorManager.Instance!=null) FieldInspectorManager.Instance.UpdateAvailableResources();
        }
    }

    public void Sold()
    {
        if(StoreManager.Instance==null || BarnManager.Instance==null) return;
        var item = StoreManager.Instance.GetCurrentItem();
        if (item == null) return;
        if (BarnManager.Instance.TryToGetResource(item, 1))
        {
            BarnManager.Instance.Put(StoreManager.Instance.GetExchangeResource(),item.GetCostForSale());
            if(FieldInspectorManager.Instance!=null) FieldInspectorManager.Instance.UpdateAvailableResources();
        }
    }
}
