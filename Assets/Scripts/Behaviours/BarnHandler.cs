﻿using UnityEngine;

public class BarnHandler : MonoBehaviour
{
    [SerializeField] private ResourceStorage currentResourceStorage;

    public void SetResourceStorage(ResourceStorage resourceStorage)
    {
        currentResourceStorage = resourceStorage;
    }
    public void GetFromBarn(ResourceItem resourceItem)
    {
        if (BarnManager.Instance == null || currentResourceStorage==null) return;
        if (currentResourceStorage.GetCapacity() < currentResourceStorage.GetCount(resourceItem)+1) return;
        if (BarnManager.Instance.TryToGetResource(resourceItem))
        {
            currentResourceStorage.PutResource(resourceItem);
        }
    }
    
    public void GetFromBarn(ResourceItem resourceItem, int count)
    {
        if (BarnManager.Instance == null || currentResourceStorage==null) return;
        if (currentResourceStorage.GetCapacity() < currentResourceStorage.GetCount(resourceItem)+count) return;
        if (BarnManager.Instance.TryToGetResource(resourceItem,count))
        {
            currentResourceStorage.PutResource(resourceItem,count);
        }
    }

    public void PutToBarn(ResourceItem resourceItem)
    {
        if (BarnManager.Instance == null || currentResourceStorage==null) return;
        if (currentResourceStorage.GetResource(resourceItem))
        {
            BarnManager.Instance.Put(resourceItem);
        }
    }
    
    public void PutToBarnAll(ResourceItem resourceItem)
    {
        if (BarnManager.Instance == null || currentResourceStorage==null) return;
        BarnManager.Instance.Put(resourceItem,currentResourceStorage.GetAll(resourceItem));
    }
    
    public void PutToBarn(ResourceItem resourceItem, int count)
    {
        if (BarnManager.Instance == null || currentResourceStorage==null) return;
        if (currentResourceStorage.GetResource(resourceItem, count))
        {
            BarnManager.Instance.Put(resourceItem, count);
        }
    }
}
