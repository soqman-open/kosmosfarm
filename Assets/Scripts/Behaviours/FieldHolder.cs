﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FieldHolder : MonoBehaviour
{
    [SerializeField] private Field currentField;
    [SerializeField] private ResourceViewUpdater fieldViewUpdater;
    [SerializeField] private ResourceViewUpdater consumedResourceViewUpdater;
    [SerializeField] private ResourceViewUpdater producedResourceViewUpdater;
    [SerializeField] private ConsumeMachine consumeMachine;
    [SerializeField] private ProduceMachine produceMachine;
    [SerializeField] private ResourceStorage consumeResourceStorage;
    [SerializeField] private ResourceStorage produceResourceStorage;
    [SerializeField] private GameObject inResource;
    [SerializeField] private GameObject outResource;
    public void SetField(Field field)
    {
        currentField = field;
        FieldBuilder.BuildField(this);
        FieldsManager.Instance.FieldPlanted();
    }
    public GameObject GetInResource()
    {
        return inResource;
    }

    public GameObject GetOutResource()
    {
        return outResource;
    }
    public Field GetField()
    {
        return currentField;
    }
    public ResourceViewUpdater GetFieldViewUpdater()
    {
        return fieldViewUpdater;
    }
    public ConsumeMachine GetConsumeMachine()
    {
        return consumeMachine;
    }

    public ProduceMachine GetProduceMachine()
    {
        return produceMachine;
    }

    public ResourceStorage GetConsumeResourceStorage()
    {
        return consumeResourceStorage;
    }
    
    public ResourceStorage GetProduceResourceStorage()
    {
        return produceResourceStorage;
    }

    public ResourceViewUpdater GetConsumedResourceViewUpdater()
    {
        return consumedResourceViewUpdater;
    }
    
    public ResourceViewUpdater GetProducedResourceViewUpdater()
    {
        return producedResourceViewUpdater;
    }

    public FieldData Save()
    {
        var fieldData = new FieldData(GetField());
        fieldData.SetMachinesData(GetMachinesData());
        fieldData.SetStoragesData(GetResourceStoragesData());
        return fieldData;
    }

    public void Load(FieldData fieldData)
    {
        SetField(fieldData.GetField());
        SetMachines(fieldData.GetMachinesData());
        SetResourceStorages(fieldData.GetResourceStoragesData());
    }

    private List<MachineData> GetMachinesData()
    {
        var machinesData =new List<MachineData>();
        
        foreach (var item in GetComponentsInChildren<Machine>(true))
        {
            machinesData.Add(item.Save());
        }
        return machinesData;
    }

    private List<ResourceStorageData> GetResourceStoragesData()
    {
        var resourceStoragesData =new List<ResourceStorageData>();
        
        foreach (var item in GetComponentsInChildren<ResourceStorage>(true))
        {
            resourceStoragesData.Add(item.Save());
        }
        return resourceStoragesData;
    }
    
    private void SetMachines(List<MachineData> machinesData)
    {
        var machines = GetComponentsInChildren<Machine>(true).ToList();
        for (var i = 0; i < machines.Count; i++)
        {
            machines[i].Load(machinesData[i]);
        }
    }
    
    private void SetResourceStorages(List<ResourceStorageData> storagesData)
    {
        var storages = GetComponentsInChildren<ResourceStorage>(true).ToList();
        for (var i = 0; i < storages.Count; i++)
        {
            storages[i].Load(storagesData[i]);
        }
    }
}
