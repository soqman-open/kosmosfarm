﻿using UnityEngine;
using UnityEngine.UI;

public class ProgressViewUpdater : MonoBehaviour
{
    [SerializeField] private Image bar;
    [SerializeField] private bool isReversed;
    [SerializeField] private Process currentProcess;
    private bool sleep;

    public void SetProcess(Process process)
    {
        currentProcess = process;
        UpdateView();
    }

    public void Update()
    {
        if (currentProcess==null) return;
        if (currentProcess.IsActive())
        {
            UpdateView();
            sleep = false;
        }
        else
        {
            if (sleep) return;
            UpdateView();
            sleep = true;
        }
    }

    private void UpdateView()
    {
        if (currentProcess == null) return;
        var progress = currentProcess.GetProgress();
        if (progress > 1) progress = 1f;
        if (progress < 0) progress = 0f;
        if (isReversed)
        {
            progress = progress==0?0:1 - progress;
            
        }
        bar.fillAmount = progress;
    }
}
