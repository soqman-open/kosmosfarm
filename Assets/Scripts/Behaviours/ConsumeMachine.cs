﻿using UnityEngine;
public class ConsumeMachine : Machine, OnResourceChangedListener 
{
    [Header("dependencies")]
    [SerializeField] private ResourceStorage resourceStorage;

    [Header("settings")]
    [SerializeField] private ResourceItem consumeResource;
    [SerializeField] private Field field;
    
    private void Awake()
    {
        resourceStorage.RegisterOnResourceChangeListener(this);
    }

    private void OnDestroy()
    {
        resourceStorage.UnregisterOnResourceChangedListener(this);
    }
    protected override bool CheckLinkedMachine()
    {
        return true;
    }

    protected override bool CheckConditions()
    {
        return resourceStorage != null && consumeResource != null;
    }

    protected override bool PreWork()
    {
        return resourceStorage.GetResource(consumeResource);
    }

    protected override void SetProcessParams()
    {
        if (process is Timer && consumeResource!=null)
        { 
            ((Timer)process).SetTime(consumeResource.GetBaseConsumeTime()*field.GetConsumptionTimeCoefficient());
        }
    }

    protected override bool PostWork()
    {
        return true;
    }

    public void Init(Field field)
    {
        this.field = field;
        consumeResource = field.GetConsumeResource();
        resourceStorage.SetCapacity(field.GetConsumeCapacity());
        Kick();
    }
    public void OnResourceChanged()
    {
        if (GetStatus() == Status.Undefined) return;
        Kick();
    }
}
