﻿public static class FieldBuilder
{
    public static void BuildField(FieldHolder fieldHolder)
    {
        if(fieldHolder.GetFieldViewUpdater()!=null)fieldHolder.GetFieldViewUpdater()?.UpdateView(fieldHolder.GetField());
        
        if (fieldHolder.GetField().GetConsumeResource() == null)
        {
            if(fieldHolder.GetInResource()!=null) fieldHolder.GetInResource().SetActive(false);
            if(fieldHolder.GetProduceMachine()!=null) fieldHolder.GetProduceMachine().LinkMachine(null);
        }
        else
        {
            if(fieldHolder.GetInResource()!=null) fieldHolder.GetInResource()?.SetActive(true);
            if(fieldHolder.GetConsumeMachine()!=null) fieldHolder.GetConsumeMachine().Init(fieldHolder.GetField());
            if(fieldHolder.GetProduceMachine()!=null && fieldHolder.GetConsumeMachine()!=null) fieldHolder.GetProduceMachine().LinkMachine(fieldHolder.GetConsumeMachine());
            if( fieldHolder.GetConsumedResourceViewUpdater()!=null) fieldHolder.GetConsumedResourceViewUpdater().UpdateView(fieldHolder.GetField().GetConsumeResource());
        }

        if (fieldHolder.GetField().GetProduceResource() == null)
        {
            if(fieldHolder.GetOutResource()!=null) fieldHolder.GetOutResource().SetActive(false);
        }
        else
        {
            if(fieldHolder.GetOutResource()!=null)fieldHolder.GetOutResource().SetActive(true);
            if(fieldHolder.GetProduceMachine()!=null)fieldHolder.GetProduceMachine().Init(fieldHolder.GetField());
            if(fieldHolder.GetProducedResourceViewUpdater())fieldHolder.GetProducedResourceViewUpdater().UpdateView(fieldHolder.GetField().GetProduceResource());
        }
    }
}
