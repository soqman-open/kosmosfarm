﻿using UnityEngine;
public class LoadFieldFromResources : MonoBehaviour
{
    public OneResourceStorage storage;
    public ResourceStorageData data;
    
    public void Load()
    {
        storage.Load(data);
    }
    
    public void Save()
    {
        data=storage.Save();
    }
}
