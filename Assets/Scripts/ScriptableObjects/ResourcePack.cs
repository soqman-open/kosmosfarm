﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newResourcePack", menuName = "Data/ResourcePack", order = 1)]
public class ResourcePack : ScriptableObject
{
    [SerializeField] private List<ResourceItem> resources;

    public List<ResourceItem> GetResources()
    {
        return resources;
    }

    public ResourceItem GetResource(int n)
    {
        return resources[n];
    }
}
