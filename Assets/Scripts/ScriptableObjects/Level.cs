﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

[CreateAssetMenu(fileName = "newLevel", menuName = "Data/Level", order = 1)]
public class Level : ScriptableObject
{
    [SerializeField] private FieldsPack fieldsPack;
    [SerializeField] private ResourcePack productsPack;
    [SerializeField] private ResourcePack resourcesPack;
    [SerializeField] private Color backgroundColor;
    [SerializeField] private string rulesFileName;
    private RulesData rules;

    public FieldsPack GetFieldsPack()
    {
        return fieldsPack;
    }

    public ResourcePack GetProductsPack()
    {
        return productsPack;
    }

    public ResourcePack GetResourcesPack()
    {
        return resourcesPack;
    }

    public Color GetColor()
    {
        return backgroundColor;
    }

    private void Awake()
    {
        LoadRulesFromURL();
    }

    public RulesData GetRules()
    {
        return rules;
    }

    private void LoadRulesFromFile()
    {
        var path = Path.Combine(SaveLoadUtility.GetRulesPath(), rulesFileName);
        Debug.Log("PATH IS"+path);
        if (!File.Exists(path)) return;
        rules = JsonUtility.FromJson<RulesData>(File.ReadAllText(path));
    }
    
    private void LoadRulesFromURL()
    {
        var path = Path.Combine(SaveLoadUtility.GetRulesPath(), rulesFileName);
        using (var request = UnityWebRequest.Get(path))
        {
            request.SendWebRequest();
            while (!request.isDone)
            {
                if (request.isHttpError || request.isNetworkError) break;
            }
            rules = JsonUtility.FromJson<RulesData>(request.downloadHandler.text);
        }
    }
}
