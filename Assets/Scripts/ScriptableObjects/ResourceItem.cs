﻿using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "newResourceItem", menuName = "Data/ResourceItem", order = 1)]
public class ResourceItem : ScriptableObject, IProduceable, IConsumable, IPurchasable, ISaleable
{
    [SerializeField] protected float baseConsumingTime;
    [SerializeField] protected float baseProducingTime;
    [SerializeField] protected int baseCost;
    [SerializeField] protected int baseCostForSale;
    [SerializeField] protected Sprite picS;
    [SerializeField] protected Sprite picM;
    [SerializeField] protected Sprite picL;

    public float GetBaseProducingTime()
    {
        return baseProducingTime;
    }

    public float GetBaseConsumeTime()
    {
        return baseConsumingTime;
    }

    public virtual Sprite GetPicS()
    {
        return picS;
    }
    
    public virtual Sprite GetPicM()
    {
        return picM;
    }
    
    public virtual Sprite GetPicL()
    {
        return picL;
    }

    public float GetBaseCost()
    {
        return baseCost;
    }

    public float GetBaseCostForSale()
    {
        return baseCostForSale;
    }
    
    public int GetCost()
    {
        return baseCost;
    }

    public int GetCostForSale()
    {
        return baseCostForSale;
    }
}
 