﻿using UnityEngine;

[CreateAssetMenu(fileName = "newField", menuName = "Data/Field", order = 1)]
public class Field : ResourceItem
{
    [SerializeField] protected float productivityCoeff;
    [SerializeField] protected float consumptionTimeCoeff;

    [SerializeField] protected ResourceItem consumeResource;
    [SerializeField] protected ResourceItem produceResource;

    [SerializeField] protected int productCapacity;
    [SerializeField] protected int consumeCapacity;
    public virtual float GetProductivityCoefficient()
    {
        return productivityCoeff;
    }
    public virtual float GetConsumptionTimeCoefficient()
    {
        return consumptionTimeCoeff;
    }

    public int GetProductCapacity()
    {
        return productCapacity;
    }

    public int GetConsumeCapacity()
    {
        return consumeCapacity;
    }

    public ResourceItem GetConsumeResource()
    {
        return consumeResource;
    }

    public ResourceItem GetProduceResource()
    {
        return produceResource;
    }
}
