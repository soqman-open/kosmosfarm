﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newFieldsPack", menuName = "Data/FieldsPack", order = 1)]
public class FieldsPack : ScriptableObject
{
    [SerializeField] private List<Field> fields;
    [SerializeField] private int x;

    public int GetX()
    {
        return x;
    }

    public List<Field> GetFields()
    {
        return fields;
    }

    public Field GetField(int n)
    {
        return fields[n];
    }
}
