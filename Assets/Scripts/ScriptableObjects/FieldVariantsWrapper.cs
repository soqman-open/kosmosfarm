﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newFieldsWrapper", menuName = "Data/FieldVariantWrapper", order = 1)]
public class FieldVariantsWrapper : Field
{
    [Header("settings")]
    [SerializeField] private List<Field> fieldVariants;

    public Field GetRandomVariant()
    {
        if (fieldVariants.Count == 0) return this;
        var variant = Instantiate(fieldVariants[Random.Range(0, fieldVariants.Count)]);
        variant.name = name;
        return variant;
    }
    
    public Field GetVariant(int index)
    {
        if (index > fieldVariants.Count || index < 0)
        {
            return this;
        }
        return fieldVariants[index];
    }

    
}
