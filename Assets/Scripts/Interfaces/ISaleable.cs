﻿public interface ISaleable
{
    float GetBaseCostForSale();
}
