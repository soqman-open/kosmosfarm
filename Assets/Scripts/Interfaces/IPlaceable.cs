﻿using UnityEngine;

public interface IPlaceable
{
    GameObject GetPrefabForFields();
}
