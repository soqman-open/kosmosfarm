﻿using UnityEngine;

public interface IPurchasable
{
    float GetBaseCost();
    Sprite GetPicS();
}
