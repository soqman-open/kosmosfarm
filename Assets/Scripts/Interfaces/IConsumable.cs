﻿public interface IConsumable
{
    float GetBaseConsumeTime();
}
