﻿public interface IProduceable
{
     float GetBaseProducingTime();
}
